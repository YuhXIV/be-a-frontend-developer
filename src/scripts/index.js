import '../styles/index.scss';

const numberOfPictures = 100;
var shownPictures = 6;
var searchQuery = "asian girl";
var photos;
var apiUrl = `https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=${searchQuery}&per_page=${numberOfPictures}&page=1`;
const apiKey = `563492ad6f9170000100000156cfe96f3c4c45759f2e861b998dacf2`;

$(document).ready(()=>{
    $.ajax({
        url: apiUrl,
        type: 'GET',
        'headers': {
            'Authorization': `Bearer ${apiKey}`
        } 
    }).done(data => {
        photos = data.photos;
        renderPhotos(photos);
    });
});

function getPhotos() {
    apiUrl = `https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=${searchQuery}&per_page=${numberOfPictures}&page=1`;
    $.ajax({
        url: apiUrl,
        type: 'GET',
        'headers': {
            'Authorization': `Bearer ${apiKey}`
        } 
    }).done(data => {
        photos = data.photos;
        document.getElementById("row").innerHTML = "";
        renderPhotos(photos);
    });
}

window.helloWorld = function() {
    console.log('HEllooOooOOo!');
};

function renderPhotos(photos) {
    let col1 = '<div class="col-4 md-4">';
    let col2 = '<div class="col-4 md-4">';
    let col3 = '<div class="col-4 md-4">';
    let height1 = 0;
    let height2 = 0;
    let height3 = 0;
    $.each(photos, (i, photo)=>{
        if(i < shownPictures) {
            if(height1 <= height2 && height1 <= height3) {
                col1 += createCard(photo);
                height1 += photo.height/photo.width;
            }
            else if(height2 <= height3 && height2 <= height1) {
                col2 += createCard(photo);
                height2 += photo.height/photo.width;
            }
            else if(height3 <= height1 && height3 <= height2) {
                col3 += createCard(photo);
                height3 += photo.height/photo.width;
            }
        }
        if(i==(photos.length-1)) {
            document.getElementById("row").innerHTML += col1 + '</div>' + col2 + '</div>' + col3 + '</div>';
        }
    });
}; 

function createCard(photo) {
    return `
    <div class="top-padding">
        <div class="card" style="18rem;">
            <div class="card-img">`
                //+`<img class="card-img-top" src="${photo.src.portrait}"/>
                + `<img class="card-img-top" src="${photo.src.large2x}"/>  
                <div class="card-body">
                    <a id="text" href="${photo.photographer_url}">${photo.photographer}</a>
                </div>
            </div>
        </div>
    </div>
    `;
};

$('#number-of-picture').keypress(function(event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
        if(parseInt($('#number-of-picture').val()) > 0) {
            shownPictures = $('#number-of-picture').val();
        }
        document.getElementById("row").innerHTML = "";
        renderPhotos(photos);
	}
});

$('#search-button').click(function(event) {
    searchQuery = $('input').val();
    if(searchQuery === "") {
        searchQuery = "asian girl";
    }
    getPhotos();
});

$('#input').keypress(function(event){
	searchQuery = $('input').val();
    if(searchQuery === "") {
        searchQuery = "asian girl";
    }
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == '13'){
		getPhotos();
	}
});

