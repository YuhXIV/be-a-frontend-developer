# Be a frontend developer

- [x] The website should serve the images dynamically from the Pexels API using jQuery.
- [x] The website should look professional and make use of Twitter Bootstrap 4.
- [x] The website should be deployed using Heroku or a similar hosting solution where it is publicly addressable online.

deployment link: https://pexels-picture-exhibition.herokuapp.com/

# Setup

### Node.js

Run this command to install all necessary dependency to run this project

```npm install```

### Run project

To get the project to run, run this command. It will automatically open http://localhost:8080/ on your browser. In case of not open, just copy-paste the link in to your web browser.

```npm run dev```